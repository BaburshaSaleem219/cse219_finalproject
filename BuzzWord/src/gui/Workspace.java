package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzWordController;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static buzzword.BuzzWordProperites.*;
//import static settings.InitializationParameters.APP_PROPERTIESS_XML;
/**
 * @author Babursha Saleem
 */
public class Workspace extends AppWorkspaceComponent {

        AppTemplate app; // the actual application
        AppGUI      gui; // the GUI inside which the application sits


        Label             guiHeadingLabel;
        BorderPane        headPane;
        BorderPane        bodyPane;
        ToolBar           footToolbar;
        GridPane          buzzLetters;
        HBox              gameSpace;
        VBox              loginDetails;
        VBox              modeSelection;
        Button            createProfile;
        Button            loginButton;
        Button            closeButton;
        Button            startPlaying;
        Button            selectMode;
        Button            gamemode1;
        Button            gamemode2;
        Button            gamemode3;
        Button            homeButton;
        VBox              levelLabel;
        HBox              timeRemaining;
        HBox              targetWord;
        VBox              points;
        VBox              guesses;
        VBox              guessesPoints;
        HBox              guessesInfo;
        Button            playButton;
        Button            pauseButton;
        Button            test;
        VBox              gameContents;
        Label             time;

        BuzzWordController controller;
        public Canvas       canvas;
        GraphicsContext     gc;

        /**
         * Constructor for initializing the workspace, note that this constructor
         * will fully setup the workspace user interface for use.
         *
         * @param initApp The application this workspace is part of.
         * @throws IOException Thrown should there be an error loading application
         *                     data for setting up the user interface.
         */
        public Workspace(AppTemplate initApp) throws IOException {
            app = initApp;
            gui = app.getGUI();
            controller = (BuzzWordController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
            layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
            setupHandlers(); // ... and set up event handling
            gui.getAppPane().setTop(null);
            gui.getAppPane().setCenter(workspace);
        }

        private void layoutGUI() {
            PropertyManager propertyManager = PropertyManager.getManager();
            guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
            guiHeadingLabel.setTextFill(Color.WHITE);
            guiHeadingLabel.setStyle("-fx-font: 24 arial;");

            test = new Button("Test Gameplay GUI");

            createProfile = new Button("Create New Profile");
            createProfile.setMinWidth(180);
            createProfile.setStyle("-fx-background-color: grey;");
            createProfile.setTextFill(Color.WHITE);

            loginButton = new Button("Login");
            loginButton.setMinWidth(180);
            loginButton.setStyle("-fx-background-color: grey;");
            loginButton.setTextFill(Color.WHITE);

            closeButton = new Button("X");
            closeButton.setStyle("-fx-background-color: transparent;");
            closeButton.setTextFill(Color.WHITE);

            startPlaying = new Button("Start Playing");
            startPlaying.setVisible(false);
            startPlaying.setMinWidth(180);
            startPlaying.setStyle("-fx-background-color: grey;");
            startPlaying.setTextFill(Color.WHITE);

            homeButton = new Button("Home");
            homeButton.setMinWidth(180);
            homeButton.setStyle("-fx-background-color: grey;");
            homeButton.setTextFill(Color.WHITE);


            selectMode = new Button("Select Mode");
            selectMode.setMinWidth(180);
            selectMode.setStyle("-fx-background-color: grey;");
            selectMode.setTextFill(Color.WHITE);

            gamemode1 = new Button("English Dictionary");
            gamemode1.setMinWidth(180);
            gamemode1.setStyle("-fx-background-color: grey;");
            gamemode1.setTextFill(Color.RED);

            gamemode2 = new Button("Places");
            gamemode2.setMinWidth(180);
            gamemode2.setStyle("-fx-background-color: grey;");
            gamemode2.setTextFill(Color.RED);

            gamemode3 = new Button("Science");
            gamemode3.setMinWidth(180);
            gamemode3.setStyle("-fx-background-color: grey;");
            gamemode3.setTextFill(Color.RED);

            modeSelection = new VBox();
            modeSelection.getChildren().addAll(selectMode,gamemode1,gamemode2,gamemode3);

            canvas = new Canvas();
            canvas.setHeight(275);
            canvas.setWidth(275);

            buzzLetters = new GridPane();
            ColumnConstraints column1 = new ColumnConstraints();
            column1.setHalignment(HPos.CENTER);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.setHgap(50);
            buzzLetters.setVgap(40);
            int counter = 0;
            Label b = new Label("B");
            b.setTextFill(Color.WHITE);
            Label u = new Label("U");
            u.setTextFill(Color.WHITE);
            Label z = new Label("Z");
            z.setTextFill(Color.WHITE);
            Label z1 = new Label("Z");
            z1.setTextFill(Color.WHITE);
            Label w = new Label("W");
            w.setTextFill(Color.WHITE);
            Label o = new Label("O");
            o.setTextFill(Color.WHITE);
            Label r = new Label("R");
            r.setTextFill(Color.WHITE);
            Label d = new Label("D");
            d.setTextFill(Color.WHITE);
            for(int i = 0;i <4;i++) {
                for (int j = 0; j < 4; j++) {
                    buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22,Paint.valueOf("grey")),i, j);
                    if (i == 0 && j ==0)
                        buzzLetters.add(b,i,j);
                    if(i == 0 && j == 1)
                        buzzLetters.add(u,i,j);
                    if (i == 1 && j ==0)
                        buzzLetters.add(z,i,j);
                    if(i == 1 && j == 1)
                        buzzLetters.add(z1,i,j);

                    if (i == 2 && j ==2)
                        buzzLetters.add(w,i,j);
                    if(i == 2 && j == 3)
                        buzzLetters.add(r,i,j);
                    if (i == 3 && j ==2)
                        buzzLetters.add(o,i,j);
                    if(i == 3 && j == 3)
                        buzzLetters.add(d,i,j);
                }
            }
            for(int i = 0;i <24;i++)
                buzzLetters.getChildren().get(i).setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.7), 10, 0.5, 2.0, 2.0);");

            headPane = new BorderPane();
            headPane.setPadding(new Insets(5,5,5,5));
            headPane.setTop(closeButton);
            headPane.setAlignment(headPane.getTop(),Pos.TOP_RIGHT);
            headPane.setCenter(guiHeadingLabel);
            headPane.setLeft(test);
            headPane.setMinHeight(10);

            gameSpace = new HBox();
            gameSpace.setPadding(new Insets(50,0,0,0));
            gameSpace.getChildren().add(buzzLetters);
            gameSpace.setMaxWidth(500);

            loginDetails = new VBox();
            loginDetails.setSpacing(20);

            loginDetails.getChildren().setAll(createProfile,loginButton,modeSelection,startPlaying);
            loginDetails.setAlignment(Pos.TOP_CENTER);
            loginDetails.getChildren().get(2).setVisible(false);

            bodyPane = new BorderPane();
            bodyPane.setLeft(loginDetails);
            bodyPane.setCenter(gameSpace);
            bodyPane.setPadding(new Insets(10,10,10,10));

            HBox blankBoxLeft  = new HBox();
            HBox blankBoxRight = new HBox();
            HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
            HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
            //footToolbar = new ToolBar(blankBoxLeft, playButton, blankBoxRight);

            workspace = new VBox();
            workspace.setStyle("-fx-background-color: lightgrey;");
            workspace.getChildren().addAll(headPane,bodyPane);
        }

        private void setupHandlers() {
            loginButton.setOnMouseClicked(e -> controller.login());
            createProfile.setOnMouseClicked( e -> controller.newUser());
            closeButton.setOnMouseClicked(e -> controller.handleExitRequest());
            startPlaying.setOnMouseClicked(e -> controller.play());
            test.setOnMouseClicked(e -> this.gamePlayGUI());
        }

        /**
         * This function specifies the CSS for all the UI components known at the time the workspace is initially
         * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
        */

        @Override
        public void initStyle() {
            PropertyManager propertyManager = PropertyManager.getManager();

            gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
            gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
            gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

            ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
            toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
            toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

            workspace.getStyleClass().add(CLASS_BORDERED_PANE);
            guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

        }


        /** This function reloads the entire workspace */
        @Override
        public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
        }

        public Button getStartPlaying(){return startPlaying;}

        public VBox loginDetails() {
            return loginDetails;
        }

        public Button getcreateProfile() {
        return createProfile;
    }

        public Button getloginButton() {
        return loginButton;
    }

        public VBox getModeSelection(){return modeSelection;}

        public Button getGamemode1(){return gamemode1;}

        public Button getGamemode2(){return gamemode2;}

        public Button getGamemode3(){return gamemode3;}

        public void reinitialize() {
            /*
            guessedLetters = new HBox();
            guessedLetters.setStyle("-fx-background-color: transparent;");
            remainingGuessBox = new HBox();
            gameTextsPane = new VBox();
            gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);
            bodyPane.getChildren().setAll(figurePane, gameTextsPane);
            */

    }

        public void levelSelectionGUI() {

            getModeSelection().setVisible(false);
            startPlaying.setVisible(false);
            loginDetails.getChildren().remove(2, 3);
            loginDetails.getChildren().add(2, homeButton);

            levelLabel = new VBox();
            Label lvl = new Label("English Dictionary");
            lvl.setTextFill(Color.WHITE);
            lvl.setStyle("-fx-font: 22 arial; -fx-underline: true;");


            bodyPane.setCenter(null);
            gameSpace.getChildren().remove(0);

            buzzLetters = new GridPane();
            ColumnConstraints column1 = new ColumnConstraints();
            column1.setHalignment(HPos.CENTER);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.getColumnConstraints().add(column1);
            buzzLetters.setHgap(50);
            buzzLetters.setVgap(40);
            int counter = 0;
            Label one = new Label("1");
            one.setTextFill(Color.WHITE);
            Label two = new Label("2");
            two.setTextFill(Color.WHITE);
            Label three = new Label("3");
            three.setTextFill(Color.WHITE);
            Label four = new Label("4");
            four.setTextFill(Color.WHITE);

            buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 0,0);
            buzzLetters.add(one,0,0);
            buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 1,0);
            buzzLetters.add(two,1,0);
            buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 2,0);
            buzzLetters.add(three,2,0);
            buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 3,0);
            buzzLetters.add(four,3,0);

            for (int i = 0; i < 8; i++)
                buzzLetters.getChildren().get(i).setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.7), 10, 0.5, 2.0, 2.0);");

            levelLabel.getChildren().addAll(lvl,buzzLetters);
            levelLabel.setAlignment(Pos.CENTER);
            levelLabel.setSpacing(30);
            gameSpace.getChildren().add(levelLabel);
            bodyPane.setPadding(new Insets(10,10,10,10));

            bodyPane.setCenter(gameSpace);
        }

    public void levelSelectionGUI2() {

        getModeSelection().setVisible(false);
        startPlaying.setVisible(false);
        loginDetails.getChildren().remove(2, 3);
        loginDetails.getChildren().add(2, homeButton);

        levelLabel = new VBox();
        Label lvl = new Label("Places");
        lvl.setTextFill(Color.WHITE);
        lvl.setStyle("-fx-font: 22 arial; -fx-underline: true;");


        bodyPane.setCenter(null);
        gameSpace.getChildren().remove(0);

        buzzLetters = new GridPane();
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.setHgap(50);
        buzzLetters.setVgap(40);
        int counter = 0;
        Label one = new Label("1");
        one.setTextFill(Color.WHITE);
        Label two = new Label("2");
        two.setTextFill(Color.WHITE);
        Label three = new Label("3");
        three.setTextFill(Color.WHITE);
        Label four = new Label("4");
        four.setTextFill(Color.WHITE);

        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 0,0);
        buzzLetters.add(one,0,0);
        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 1,0);
        buzzLetters.add(two,1,0);
        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 2,0);
        buzzLetters.add(three,2,0);
        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 3,0);
        buzzLetters.add(four,3,0);

        for (int i = 0; i < 8; i++)
            buzzLetters.getChildren().get(i).setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.7), 10, 0.5, 2.0, 2.0);");

        levelLabel.getChildren().addAll(lvl,buzzLetters);
        levelLabel.setAlignment(Pos.CENTER);
        levelLabel.setSpacing(30);
        gameSpace.getChildren().add(levelLabel);
        bodyPane.setPadding(new Insets(10,10,10,10));

        bodyPane.setCenter(gameSpace);
    }

    public void levelSelectionGUI3() {

        getModeSelection().setVisible(false);
        startPlaying.setVisible(false);
        loginDetails.getChildren().remove(2, 3);
        loginDetails.getChildren().add(2, homeButton);

        levelLabel = new VBox();
        Label lvl = new Label("Science");
        lvl.setTextFill(Color.WHITE);
        lvl.setStyle("-fx-font: 22 arial; -fx-underline: true;");


        bodyPane.setCenter(null);
        gameSpace.getChildren().remove(0);

        buzzLetters = new GridPane();
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.setHgap(50);
        buzzLetters.setVgap(40);
        int counter = 0;
        Label one = new Label("1");
        one.setTextFill(Color.WHITE);
        Label two = new Label("2");
        two.setTextFill(Color.WHITE);
        Label three = new Label("3");
        three.setTextFill(Color.WHITE);
        Label four = new Label("4");
        four.setTextFill(Color.WHITE);

        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 0,0);
        buzzLetters.add(one,0,0);
        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 1,0);
        buzzLetters.add(two,1,0);
        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 2,0);
        buzzLetters.add(three,2,0);
        buzzLetters.add(new Circle(canvas.getWidth(), canvas.getHeight(), 22, Paint.valueOf("grey")), 3,0);
        buzzLetters.add(four,3,0);

        for (int i = 0; i < 8; i++)
            buzzLetters.getChildren().get(i).setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.7), 10, 0.5, 2.0, 2.0);");

        levelLabel.getChildren().addAll(lvl,buzzLetters);
        levelLabel.setAlignment(Pos.CENTER);
        levelLabel.setSpacing(30);
        gameSpace.getChildren().add(levelLabel);
        bodyPane.setPadding(new Insets(10,10,10,10));

        bodyPane.setCenter(gameSpace);
    }

    public void gamePlayGUI(){


        gameContents = new VBox();
        gameContents.setSpacing(15);
        File find = new File("C:\\Users\\Babu\\Desktop\\Cse219Hw4\\BuzzWord\\resources\\images\\play.png");
        Image play = new Image(find.toURI().toString());

        guessesPoints = new VBox();
        guessesInfo = new HBox();
        targetWord = new HBox();
        points = new VBox();
        timeRemaining = new HBox();
        guesses = new VBox();

        time = new Label("Time Remaining: 40 Seconds");
        time.setTextFill(Color.RED);
        time.setPadding(new Insets(10,10,10,10));
        timeRemaining.setMinHeight(30);
        timeRemaining.setMinWidth(210);

        timeRemaining.getChildren().add(time);
        timeRemaining.setAlignment(Pos.CENTER_LEFT);

        Label AB = new Label("B U");
        AB.setTextFill(Color.WHITE);
        AB.setPadding(new Insets(10,10,10,10));
        targetWord.setMinHeight(30);
        targetWord.setMaxWidth(120);
        targetWord.getChildren().addAll(AB);
        targetWord.setAlignment(Pos.CENTER_LEFT);

        Label testguess1 = new Label("WAR");
        Label total = new Label("TOTAL:");
        testguess1.setPadding(new Insets(10,10,10,10));
        total.setPadding(new Insets(10,10,10,10));
        testguess1.setTextFill(Color.WHITE);
        total.setTextFill(Color.WHITE);
        guesses.getChildren().addAll(testguess1,total);

        Label testtotalpoints = new Label("10");
        Label testpoints1 = new Label("10");
        testtotalpoints.setPadding(new Insets(10,10,10,10));
        testpoints1.setPadding(new Insets(10,10,10,10));
        testtotalpoints.setTextFill(Color.WHITE);
        testpoints1.setTextFill(Color.WHITE);
        guessesPoints.getChildren().addAll(testpoints1,testtotalpoints);
        guessesInfo.getChildren().addAll(guesses,guessesPoints);
        guessesInfo.setMinHeight(200);
        guessesInfo.setMinWidth(100);
        guessesInfo.setSpacing(50);

        Label target = new Label("Target");
        Label point = new Label("75 Points");
        target.setTextFill(Color.WHITE);
        point.setTextFill(Color.WHITE);
        point.setPadding(new Insets(10,10,10,10));
        points.setMinHeight(100);
        points.setMinWidth(50);

        points.getChildren().addAll(target,point);

        targetWord.setStyle("-fx-background-color: grey;");
        timeRemaining.setStyle("-fx-background-color: white; -fx-background-radius: 5;");
        guesses.setStyle("-fx-background-color: grey;");
        guessesPoints.setStyle("-fx-background-color: grey;");
        guessesInfo.setStyle("-fx-background-color: grey;");
        points.setStyle("-fx-background-color: grey;-fx-background-radius: 7;");

        Label level = new Label("Level 4");
        level.setTextFill(Color.WHITE);
        level.setStyle("-fx-font: 22 arial; -fx-underline: true;");
        gameSpace.getChildren().remove(0);



        playButton = new Button();
        playButton.setGraphic(new ImageView(play));
        playButton.setStyle("-fx-background-color: transparent;");

        buzzLetters = new GridPane();
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.getColumnConstraints().add(column1);
        buzzLetters.setHgap(50);
        buzzLetters.setVgap(40);
        int counter = 0;
        Label b = new Label("B");
        b.setTextFill(Color.WHITE);
        Label u = new Label("U");
        u.setTextFill(Color.WHITE);
        Label z = new Label("Z");
        z.setTextFill(Color.WHITE);
        Label z1 = new Label("Z");
        z1.setTextFill(Color.WHITE);
        Label w = new Label("W");
        w.setTextFill(Color.WHITE);
        Label o = new Label("O");
        o.setTextFill(Color.WHITE);
        Label r = new Label("R");
        r.setTextFill(Color.WHITE);
        Label d = new Label("D");
        d.setTextFill(Color.WHITE);
        for(int i = 0;i <4;i++) {
            for (int j = 0; j < 4; j++) {
                buzzLetters.add(new Circle(canvas.getWidth()-800, canvas.getHeight()-800, 22,Paint.valueOf("grey")),i, j);
                if (i == 0 && j ==0)
                    buzzLetters.add(b,i,j);
                if(i == 0 && j == 1)
                    buzzLetters.add(u,i,j);
                if (i == 1 && j ==0)
                    buzzLetters.add(z,i,j);
                if(i == 1 && j == 1)
                    buzzLetters.add(z1,i,j);

                if (i == 2 && j ==2)
                    buzzLetters.add(w,i,j);
                if(i == 2 && j == 3)
                    buzzLetters.add(r,i,j);
                if (i == 3 && j ==2)
                    buzzLetters.add(o,i,j);
                if(i == 3 && j == 3)
                    buzzLetters.add(d,i,j);
            }
        }
        for(int i = 0;i <24;i++)
            buzzLetters.getChildren().get(i).setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.7), 10, 0.5, 2.0, 2.0);");

        gc = canvas.getGraphicsContext2D();
        gc.strokeLine(canvas.getHeight(),canvas.getWidth(),canvas.getHeight(),0);
        gc.strokeLine(0,0,0,canvas.getHeight());
        gc.strokeLine(5,5,canvas.getWidth(),5);
        gc.strokeLine(canvas.getHeight()-5,canvas.getWidth()-5,0,canvas.getHeight()-5);
        gc.strokeLine(0,canvas.getWidth(),0,0);
        gc.strokeLine(canvas.getHeight()-90,canvas.getWidth(),canvas.getHeight()-90,0);
        gc.strokeLine(canvas.getHeight()-180,canvas.getWidth(),canvas.getHeight()-180,0);
        gc.strokeLine(0,canvas.getWidth()-90,canvas.getHeight(),canvas.getHeight()-90);
        gc.strokeLine(0,canvas.getWidth()-180,canvas.getHeight(),canvas.getHeight()-180);
        gc.setFill(Color.BLACK);
        StackPane test = new StackPane();
        test.getChildren().addAll(canvas,buzzLetters);
        gameContents.getChildren().addAll(timeRemaining,targetWord,guessesInfo,points);
        levelLabel.getChildren().remove(1);
        levelLabel.getChildren().addAll(test,level,playButton);
        gameSpace.getChildren().addAll(levelLabel,gameContents);
        gameSpace.setSpacing(50);
        bodyPane.setCenter(gameSpace);
        //bodyPane.setRight(gameContents);

    }
}
