package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import components.AppComponentsBuilder;
import components.AppFileComponent;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.geometry.*;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.awt.*;
import java.awt.TextField;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.InputMismatchException;


import static settings.AppPropertyType.*;
/**
 * @author Babursha Saleem
 */
public class BuzzWordController implements FileController{

        private AppTemplate appTemplate; // shared reference to the application
        //private GameData    gamedata;    // shared reference to the game being played, loaded or saved
        //private Text[]      progress;    // reference to the text area for the word
        //private boolean     success;     // whether or not player was successful
        //private int         discovered;  // the number of letters already discovered
        private Button      createProfile;
        private Button      loginButton;
        private Button      playButton;
        //private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
        //private boolean     gameover;    // whether or not the current game is already over
        private boolean     savable;
        //private Path        workFile;
        //public File                  currentWorkFile; // the file on which currently work is being done
        //public SimpleBooleanProperty saved;           // whether or not changes have been saved
        //public boolean      hintUsed=false;
        //private Button      hintButton;
        public Canvas       canvas;
        GraphicsContext     gc;
        //public int          numberWrong = 0;
        //public boolean      hintAvailable =true;

        public BuzzWordController(AppTemplate appTemplate, Button create, Button login) {
            this(appTemplate);
            this.createProfile = create;
            this.loginButton = login;
        }

        public BuzzWordController(AppTemplate appTemplate) {
            this.appTemplate = appTemplate;
        }

        /*
        public void enableGameButton() {
            if (gameButton == null) {
                Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
                gameButton = workspace.getStartGame();
            }
            gameButton.setDisable(false);
        }
        */


        public void newUser() {


        }


        public void login() {

            Button confirm = new Button("Ok");

            HBox username = new HBox();
            Label user = new Label("Profile Name");
            user.setPadding(new Insets(0,25,0,0));
            user.setTextFill(Color.WHITE);
            username.setAlignment(Pos.CENTER);
            username.setSpacing(10);
            username.getChildren().addAll(user,new javafx.scene.control.TextField());

            HBox password = new HBox();
            password.setPadding(new Insets(0,0,25,0));
            Label pass = new Label("Profile Password");
            pass.setTextFill(Color.WHITE);
            password.setAlignment(Pos.CENTER);
            password.setSpacing(10);
            password.getChildren().addAll(pass,new javafx.scene.control.TextField());

            VBox credentials = new VBox();
            credentials.setAlignment(Pos.CENTER);
            credentials.setSpacing(20);
            credentials.getChildren().addAll(username,password,confirm);
            credentials.setStyle("-fx-background-color: black;-fx-opacity: .8;");
            Stage login = new Stage();
            login.initStyle(StageStyle.TRANSPARENT);
            Scene scene = new Scene(credentials,500,250,Color.TRANSPARENT);

            confirm.setOnMouseClicked(e -> login.close());
            login.setScene(scene);
            login.show();

        }

        public void loginVerify(){


        }


        public void BuzzwordGraphics(int numberWrong){
            String outlineColor = "#000000";
            int head = 100;

            canvas.setStyle("-fx-background-color: white");
            gc = canvas.getGraphicsContext2D();
            canvas.setHeight(800);
            canvas.setWidth(800);
            gc.setFill(Paint.valueOf(outlineColor));
            if(numberWrong == 1)
                gc.fillRect(canvas.getWidth()-790, canvas.getHeight() - 250, 550, 10);
            if (numberWrong == 2)
                gc.fillRect(canvas.getWidth()-790, canvas.getHeight() - 750 , 10, 500);
            if (numberWrong == 3)
                gc.fillRect(canvas.getWidth()-790, canvas.getHeight() -750, 300, 5);
            if (numberWrong == 4)
                gc.fillRect(canvas.getWidth()-490, canvas.getHeight()-750, 5, 50);
            if (numberWrong == 5) {
                gc.setLineWidth(5);
                gc.strokeOval(canvas.getWidth() - 540, canvas.getHeight() - 700, head, head);
            }
            if (numberWrong == 6)
                gc.fillRect(canvas.getWidth()-490, canvas.getHeight()-600,5,200);
            if (numberWrong == 7)
                gc.strokeLine(canvas.getWidth()-490, canvas.getHeight()-550,canvas.getWidth()-550,canvas.getHeight()-500);
            if (numberWrong == 8)
                gc.strokeLine(canvas.getWidth()-485, canvas.getHeight()-550, canvas.getWidth()-425, canvas.getHeight()-500);
            if (numberWrong == 9)
                gc.strokeLine(canvas.getWidth()-490, canvas.getHeight()-400, canvas.getWidth()-550, canvas.getHeight()-300);
            if (numberWrong == 10)
                gc.strokeLine(canvas.getWidth()-485, canvas.getHeight()-400, canvas.getWidth()-425, canvas.getHeight()-300);


        }


        @Override
        public void handleNewRequest() {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
            PropertyManager           propertyManager = PropertyManager.getManager();
            boolean                   makenew         = true;

            if (savable)
                try {
                    YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
                    dialog.show("save","Save before making new?");
                    if(dialog.getSelection().equals("Yes")) {
                        handleSaveRequest();
                        gc.clearRect(0, 0,canvas.getHeight(), canvas.getWidth());
                    }
                    if(dialog.getSelection().equals("No"))
                        gc.clearRect(0, 0,canvas.getHeight(), canvas.getWidth());

                } catch (IOException e) {

                    messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
                }
            if (makenew) {
                /*
                appTemplate.getGUI().updateWorkSpaceToolbar();
                enableHintButton();
                hintButton.setVisible(false);
                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                ensureActivatedWorkspace();                            // ensure workspace is activated
                workFile = null;                                        // new workspace has never been saved to a file
                currentWorkFile = null;
                hintUsed = false;
                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gameWorkspace.reinitialize();
                enableGameButton();
            }

            if (gameover) {
                enableHintButton();
                hintButton.setVisible(false);
                savable = false;
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gameWorkspace.reinitialize();
                hintButton.setVisible(false);
                gc.clearRect(0, 0,canvas.getHeight(), canvas.getWidth());
                enableGameButton();
            }
*/
            }
        }


        @Override
        public void handleSaveRequest() throws IOException {

            /*
            PropertyManager propertyManager = PropertyManager.getManager();
            try {

                if (currentWorkFile != null)
                    saveWork(currentWorkFile);
                else {
                    FileChooser fileChooser = new FileChooser();
                    File workFolder = new File(System.getProperty("user.dir"));
                    //URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                    // if (workDirURL == null)
                    //    throw new FileNotFoundException("Work folder not found under resources.");

                    File initialDir = new File(workFolder.toString());
                    fileChooser.setInitialDirectory(initialDir);
                    fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
                            propertyManager.getPropertyValue(WORK_FILE_EXT)));
                    File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                    File jsonSelectedFile = new File(selectedFile.toString().concat(".json"));
                    selectedFile = jsonSelectedFile;

                    if (selectedFile != null)
                        saveWork(selectedFile);
                }
            } catch (IOException ioe) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
                System.exit(1);
            }
            */

        }


        @Override
        public void handleLoadRequest() throws IOException {

            /*
            PropertyManager propertyManager = PropertyManager.getManager();
            AppMessageDialogSingleton failedload = AppMessageDialogSingleton.getSingleton();
            try {

                if(currentWorkFile != null && gamedata.getRemainingGuesses()> 0)
                    promptToSave();

                FileChooser fileChooser = new FileChooser();
                File workFolder = new File(System.getProperty("user.dir"));
                File initialDir = new File(workFolder.toString());
                fileChooser.setInitialDirectory(initialDir);
                File selectedFile = fileChooser.showOpenDialog((appTemplate.getGUI().getWindow()));

                if (!(selectedFile.toString().endsWith(".json"))) {
                    failedload.show("Error","File must be a .json with game data.");

                    handleLoadRequest();
                }

                if (selectedFile == null) {
                    throw new NullPointerException();
                }
                currentWorkFile = selectedFile;
                ObjectMapper mapper = new ObjectMapper();
                GameData data = mapper.readValue(selectedFile, GameData.class);
                if(data == null) {
                    currentWorkFile = null;
                    throw new Exception();
                }
                this.gamedata = data;
                numberWrong = data.getNumberWrong();
                hintUsed = data.gethintUsed();
                success = false;
                gameButton.setDisable(true);
                savable = true;
                discovered = 0;
                boolean[] hintTest = new boolean[256];
                int unique = 0;
                while (!(gamedata.getTargetWord().matches("[a-z]+"))) {
                    gamedata = new GameData(appTemplate);
                }
                gamedata.getTargetWord().toCharArray();
                for(int i = 0;i < gamedata.getTargetWord().length();i++) {
                    int val = gamedata.getTargetWord().charAt(i);
                    if(hintTest[val] == false) {
                        hintTest[val] = true;
                        unique++;
                    }
                }
                if(unique > 7) {
                    enableHintButton();
                    hintButton.setDisable(false);
                }
                else {
                    enableHintButton();
                    hintButton.setVisible(false);
                }
                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                gameWorkspace.reinitialize();
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
                HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
                VBox gametextPane = gameWorkspace.getGameTextsPane();
                gametextPane.setSpacing(25);

                LoadinitWordGraphics(guessedLetters);
                remains = new Label(Integer.toString(data.getRemainingGuesses()));
                remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

                for(int i = 1; i < numberWrong+1;i++){
                    hangmanGraphics(i);
                }
                Character[] array = gamedata.getGoodGuesses().toArray(new Character[gamedata.getGoodGuesses().size()]);
                Character[] bad = gamedata.getBadGuesses().toArray(new Character[gamedata.getBadGuesses().size()]);
                loadLetters();
                for(int i = 0; i < bad.length;i++)
                    letterSetup(bad[i]);
                for(int i = 0; i < array.length;i++)
                    letterSetup(array[i]);
                play();

            }
            catch(NullPointerException e1){
                AppMessageDialogSingleton noFile = AppMessageDialogSingleton.getSingleton();
                noFile.show("Error", "No file selected");
            }
            catch(Exception e1){
                AppMessageDialogSingleton noFile = AppMessageDialogSingleton.getSingleton();
                noFile.show("Error", "Data file corrupted");
            }
            */
        }


        @Override
        public void handleExitRequest() {

            System.exit(0);

            /*
            try {
                boolean exit = true;
                if(savable == false)
                    System.exit(0);
                if (savable && currentWorkFile !=null)
                    exit = promptToSave();
                if (currentWorkFile == null) {
                    YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
                    dialog.show("Save","Save before quit?");
                    if(dialog.getSelection().equals("Yes")) {
                        handleSaveRequest();
                    }
                    if(dialog.getSelection().equals("Cancel"))
                        return;
                }
                if (exit)
                    System.exit(0);
            } catch (IOException ioe) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                PropertyManager           props  = PropertyManager.getManager();
                dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
            }
            */
        }

        private void ensureActivatedWorkspace() {
            appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
        }

        private boolean promptToSave() throws IOException {
            return false;
            /*
            YesNoCancelDialogSingleton prompt = YesNoCancelDialogSingleton.getSingleton();
            prompt.show("Save", "Save before proceeding?");
            if(prompt.getSelection().equals("Yes")){
                saveWork(currentWorkFile);
                return true;

            }
            if(prompt.getSelection().equals("No"))
                return true;
            else
                return false;
                */
        }


        /**
         * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
         * updates the appropriate controls in the user interface
         *
         * @param target The file to which the work will be saved.
         * @throws IOException
         */
        private void save(Path target) throws IOException {




        }

        private void saveWork(File selectedFile) throws IOException {
            //appTemplate.getFileComponent()
            //      .saveData(appTemplate.getDataComponent(), Paths.get(selectedFile.getAbsolutePath()));
/*
            ObjectMapper mapper = new ObjectMapper();
            GameData saveTo = this.gamedata;
            saveTo.appTemplate = null;
            mapper.writeValue(selectedFile,saveTo);

            currentWorkFile = selectedFile;

            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));

            // appTemplate.getGUI().updateWorkspaceToolbar(false);
            */
        }


}
